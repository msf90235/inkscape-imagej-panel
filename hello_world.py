#!/usr/bin/env python
# coding=utf-8
#
# Copyright (C) 2021 Jerome Mutterer
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#
"""
Example exetension setting some text
"""

import inkex
from inkex import Layer, FlowRoot, FlowRegion, FlowPara, Rectangle, TextElement

class Hello(inkex.EffectExtension):
    """Prints Hello World"""
    def add_arguments(self, pars):
        pars.add_argument("-s", "--fontsize", type=float, default=10, help="+/-")
        pars.add_argument("-m", "--message", default="", help="+/-")

    def effect(self):

        parent = self.document.getroot()
        self.doc_w = self.svg.unittouu(parent.get('width'))
        self.doc_h = self.svg.unittouu(parent.get('height'))
        layer_atts = {'id': 'hello_world_layer'}
        self.my_layer = parent.add(inkex.Group(**layer_atts))
        te = TextElement()
        te.text = self.options.message
        te.style["font-size"] = str(self.options.fontsize)
        te.set('id' , 'my_label_id')
        te.set('x' , str(self.doc_w / 3))
        te.set('y' , str(self.doc_w / 2))
        self.my_layer.add(te)
        
if __name__ == '__main__':
    Hello().run()
