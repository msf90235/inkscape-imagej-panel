#!/usr/bin/env python
# coding=utf-8
#
# Copyright (C) 2021 Jerome Mutterer
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#
"""
Adds customizable lane labels to a existing rect or image 
"""

import random

import inkex
from inkex import TextElement, Rectangle, Image

labels = 'abcdefghijklmnopqrstuvwxyz'

class LaneLabels(inkex.EffectExtension):
    """Adds lane labels to figure panels"""
    def add_arguments(self, pars):
        pars.add_argument("-s", "--fontsize", type=float, default=10, help="+/-")
        pars.add_argument("-a", "--angle", type=float, default=0, help="+/-")
        pars.add_argument("-x", "--xoffset", type=float, default=5, help="+/-")
        pars.add_argument("-y", "--yoffset", type=float, default=0, help="+/-")
        pars.add_argument("-n", "--number", type=int, default=5, help="+/-")
        pars.add_argument("-p", "--prefix", default="", help="+/-")
        pars.add_argument("--tab", help="The selected UI-tab when OK was pressed")

    def effect(self):
        shape = self.svg.selection.first()
        if isinstance(shape, (Rectangle,Image)):
            w = shape.width
            n = self.options.number
            parent = self.document.getroot()
    
            layer_atts = {'id': 'lane_labels'}
            self.my_layer = parent.add(inkex.Group(**layer_atts))
            x=shape.unittouu(shape.left)
            y=shape.unittouu(shape.top)
            w=shape.unittouu(shape.width)
            
            for i in range(n):
                    te = TextElement()
                    te.text = self.options.prefix+str(i+1)
                    te.style["font-size"] = str(self.options.fontsize)
                    te.set('transform',
                      "translate("+str(x+i*w/n+self.options.xoffset)+","+str(y+self.options.yoffset)+"); rotate("+str(self.options.angle)+")")
                    self.my_layer.add(te)
    
           
if __name__ == '__main__':
    LaneLabels().run()
