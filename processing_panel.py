#!/usr/bin/env python3
# coding=utf-8
#
# Copyright (C) 2021 Jerome Mutterer and Martin Owens
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>
#
"""
Request Image from Processing sketches
"""

import os
import sys
import shlex

from base64 import encodebytes

import inkex
from inkex.elements import Image, Rectangle
from inkex.command import call

from base64 import decodebytes

from ijmacro_panel import ImageJPanel

class ProcessingPanel(ImageJPanel):
    """ProcessingPanel Panel"""
    select_all = (Image, Rectangle)

    def add_arguments(self, pars):
        pars.add_argument("--tab", help="The selected UI-tab when OK was pressed")
        pars.add_argument("--pcmd", help="processing-java program", default="processing-java")
        pars.add_argument("--pcmdopt", help="processing-java options", default="--sketch=$MACRO --run")
        pars.add_argument("--quality", type=float, default=2, help="+/-")
        self.arg_path(pars, "--images_path", "./images/", "Path to Images")
        self.arg_path(pars, "--sketch_path", "./sketch/", "Path to Sketch")

    def _process_image(self, ops, elem):
        images_file = os.path.join(ops.images_path, 'sketch.svg').replace('\\', '/')
        macros_file = os.path.join(ops.sketch_path, 'sketch.pde').replace('\\', '/')
        description = ""
        for child in elem.descendants():
            if isinstance(child, inkex.Desc):
                description = child.text

        qual_factor = ops.quality
        width = int(elem.width*qual_factor)
        height = int(elem.height*qual_factor)
        
        script = f"""
// sketch
import processing.svg.*;
size({width},{height}, SVG, "{images_file}");

{description}

exit();
"""

        # Save the script to the rscript path
        with open(macros_file, 'w') as fhl:
            fhl.write(script)

        # Inject the script file path into the RScript command
        cmdopt = ops.pcmdopt.replace("$MACRO", ops.sketch_path)

        # We build the command from the program name plus arguments
        # The arguments are built as a list to maintain security

        done = call(ops.pcmd, *(shlex.split(cmdopt)))
        
        if not os.path.isfile(images_file):
            raise inkex.AbortExtension(f"Failed to save image file '{images_file}'")

        elem = self._elem_is_image(elem,"processingpanel")
        elem.desc = description

        self._embed_or_link_image(elem,images_file,True)


if __name__ == '__main__':
    ProcessingPanel().run()
