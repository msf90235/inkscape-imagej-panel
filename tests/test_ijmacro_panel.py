# coding=utf-8

from ijmacro_panel import ImageJPanel

from inkex.tester import ComparisonMixin, TestCase

class TestImageJPanel(ComparisonMixin, TestCase):
    effect_class = ImageJPanel
    compare_file = 'input.svg'
    comparisons = [
        ('--id=target', '--embed=false'),
    ]
